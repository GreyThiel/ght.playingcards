#include <iostream>;
#include <conio.h>;

using namespace std;

//enums for rank and suit
enum Rank
{
	Two = 2,
	Three,
	Four,
	Five,
	Six,
	Seven,
	Eight,
	Nine,
	Ten,
	Jack,
	Queen,
	King,
	Ace
};

enum Suit
{
	Spades,
	Hearts,
	Clubs,
	Diamonds
};

//struct for a card
struct Card
{
	Rank rank = Two;
	Suit suit = Spades;
};

int main()
{
	Card a;
	a.rank = Jack;
	a.suit = Hearts;

	Card b;
	b.rank = Ace;
	b.suit = Spades;

	(void)_getch();
	return 0;
}